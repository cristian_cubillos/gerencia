from django.apps import AppConfig


class IaFlujoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'IA_Flujo'
