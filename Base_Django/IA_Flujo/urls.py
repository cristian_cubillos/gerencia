from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()

urlpatterns = router.urls + [
    path(r'crear_flujo/', FlujoViewSet.crear_flujo, name="flujo/crear_flujo"),
    path('crear_componente/', ComponenteViewSet.crear_componente, name="flujo/crear_componente"),
    path('crear_actividad/', ActividadViewSet.crear_actividad, name="flujo/crear_actividad"),
    path('crear_regla/', ReglaViewSet.crear_regla, name="flujo/crear_regla"),
    path('inicio_flujo/', FlujoViewSet.redic, name="flujo/inicio_flujo"),
    path('inicio_componente/', ComponenteViewSet.redic, name="flujo/inicio_componente"),
    path('inicio_actividad/', ActividadViewSet.redic, name="flujo/inicio_actividad"),
    path('inicio_regla/', ReglaViewSet.redic, name="flujo/inicio_regla"),
    path(r'editar_flujo/<int:pk>', FlujoViewSet.editar_flujo, name="flujo/editar_flujo"),
    path('editar_componente/<int:pk>', ComponenteViewSet.editar_componente, name="flujo/editar_componente"),
    path('editar_actividad/<int:pk>', ActividadViewSet.editar_actividad, name="flujo/editar_actividad"),
    path('editar_regla/<int:pk>', ReglaViewSet.editar_regla, name="flujo/editar_regla"),    
]
