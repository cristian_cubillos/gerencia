from django.apps import AppConfig


class IaProyectoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'IA_Proyecto'
