from rest_framework import serializers
from . import models

#Serializadores Proyecto

#Serializer Proyecto
class ProyectoSerializer(serializers.Serializer):
    class Meta:
        model = models.Proyecto

        fields = ('titulo', 'responsable', 'cliente', 'objetivo'
            'descripcion', 'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializer Requerimientos del proyecto
class RequerimientoSerializer(serializers.Serializer):
    class Meta:
        model = models.Requerimientos

        fields = ('requerimiento', 'proyecto_ID', 'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')  

#Serializer Elementos Flujo
class ElementoFlujoSerializer(serializers.Serializer):
    class Meta:
        model = models.Elemento_Flujo

        fields = ('flujo_ID', 'proyecto_ID', 'responsable', 'fecha_Inicio'
            'fecha_Vencimiento', 'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializer Historial Componentes
class HistorialComponenteSerializer(serializers.Serializer):
    class Meta:
        model = models.Historial_Componente

        fields = ('Componente_ID', 'elemento_Flujo_ID', 'fecha_Elemento',
            'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializer Tarea
class TareaSerializer(serializers.Serializer):
    class Meta:
        model = models.Tarea

        fields = ('nombre', 'descripcion', 'requerido',
            'historial_Componente_ID', 'responsable', 'fecha_Inicio',
            'duracion', 'actividad_ID', 'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')