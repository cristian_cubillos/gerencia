from django.apps import AppConfig


class IaUsuarioConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'IA_Usuario'
