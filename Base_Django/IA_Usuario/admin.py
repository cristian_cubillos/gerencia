from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Area)
admin.site.register(Dependencia)
admin.site.register(Cargo)
#admin.site.register(Rol)
admin.site.register(Usuario)