from rest_framework import serializers
from . import models

#Serializadores Usuario

#Serializer Area
class AreaSerializer(serializers.Serializer):
    class Meta:
        model = models.Area

        fields = ('area', 'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializer Dependencia
class DependenciaSerializer(serializers.Serializer):
    class Meta:
        model = models.Dependencia

        fields = ('dependencia', 'area_ID', 
        'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')  

#Serializer Cargo
class CargoSerializer(serializers.Serializer):
    class Meta:
        model = models.Cargo

        fields = ('cargo', 'dependencia_ID', 
        'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializer Usuario
class UsuarioSerializer(serializers.Serializer):
    class Meta:
        model = models.Usuario

        fields = ('password', 'is_superuser', 'username',
            'first_name', 'last_name', 'email',
            'is_staff', 'is_active', 'cargo_ID')
        
        read_only_fields = ('last_login', 'date_joined')

# class UsuarioCargoSerializer(serializers.Serializer):
#     class Meta:
#         model = models.Usuario

#         fields = ('cargo_ID',)
        
#Serializer Rol
# class RolSerializer(serializers.Serializer):
#     class Meta:
#         model = models.Rol

#         fields = ('rol', 'estado', 'fecha_Creacion', 'fecha_Update')
        
#         read_only_fields = ('fecha_Creacion', 'fecha_Update')