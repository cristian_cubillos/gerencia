from django.shortcuts import render
from rest_framework import viewsets
from .serializers import *
from .models import *
from .forms import *
from datetime import datetime

# Create your views here.

#Area
class AreaViewSet (viewsets.ModelViewSet):
    
    serializer_class = Area
    #permission_classes = [] Solicitud de permisos...

    def crear_usuario(request):
        if request.POST:
            form = UsuarioForm(request.POST)
            if form.is_valid:
                usuario = form.save(commit=False)
                return render(request, 'proyecto.html')
        else:
            form = UsuarioForm()
            contexto = {
                'usuarioForm' : form,
            }
            return render(request, 'crear_usuario.html', contexto)
             

#Dependencia
class DependenciaViewSet (viewsets.ModelViewSet):
    
    serializer_class = DependenciaSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_requerimiento(request):
        if request.POST:
            form = DependenciaForm(request.POST)
            if form.is_valid:
                requerimientos = form.save(commit=False)
                return render(request, 'index.html')
        else:            
            form = DependenciaForm()
            contexto = {
                'dependenciaForm' : form,
            }
            return render(request, 'crear_requerimiento.html', contexto)

#Elementos Flujo
class ElementoFlujoViewSet (viewsets.ModelViewSet):
    
    serializer_class = ElementoFlujoSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_elemento(request):
        if request.POST:
            form = Elemento_FlujoForm(request.POST)
            if form.is_valid:
                elemento = form.save(commit=False)
                return render(request, 'index.html')
        else:
            form = Elemento_FlujoForm()
            contexto = {
                'elementoForm' : form,
            }
            return render(request, 'crear_elemento.html', contexto)


#Historial Componentes
class HistorialComponenteViewSet (viewsets.ModelViewSet):
    
    serializer_class = HistorialComponenteSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_historial(request):
        if request.POST:
            form = Historial_ComponenteForm(request.POST)
            if form.is_valid:
                historial = form.save(commit=False)
                return render(request, 'index.html')
        else:
            form = Historial_ComponenteForm()
            contexto = {
                'historialForm' : form,
            }
            return render(request, 'crear_historial.html', contexto)

#Tarea
class TareaViewSet (viewsets.ModelViewSet):
    
    serializer_class = TareaSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_tarea(request):
        if request.POST:
            form = TareaForm(request.POST)
            if form.is_valid:
                tarea = form.save(commit=False)
                return render(request, 'index.html')
        else:
            form = TareaForm()
            contexto = {
                'tareaForm' : form,
            }
            return render(request, 'crear_tarea.html', contexto)

