from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()

urlpatterns = router.urls + [
    path(r'index/', index, name="index"),
    path('crear_area/', AreaViewSet.crear_Area, name="proyecto/crear_area"),
    path('crear_dependencia/', DependenciaViewSet.crear_Dependencia, name="proyecto/crear_dependencia"),
    path('crear_cargo/', CargoViewSet.crear_Cargo, name="proyecto/crear_cargo"),
    path('crear_usuario/', UsuarioViewSet.crear_Usuario, name="proyecto/crear_usuario"),
    path('crear_rol/', RolViewSet.crear_Rol, name="proyecto/crear_rol"),
]