from rest_framework import serializers
from . import models

#Serializadores Usuario

#Serializer Area
class AreaSerializer(serializers.Serializer):
    class Meta:
        model = models.Area

        fields = ('area', 'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializer Dependencia
class DependenciaSerializer(serializers.Serializer):
    class Meta:
        model = models.Dependencia

        fields = ('dependencia', 'area_ID', 
        'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')  

#Serializer Elementos Flujo
class CargoSerializer(serializers.Serializer):
    class Meta:
        model = models.Cargo

        fields = ('cargo', 'dependencia_ID', 
        'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializer Historial Componentes
class UsuarioSerializer(serializers.Serializer):
    class Meta:
        model = models.Usuario

        fields = ('identificacion', 'nombre', 'correo',
            'contrasenha', 'cargo_ID', 'rol_ID',
            'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializer Tarea
class RolSerializer(serializers.Serializer):
    class Meta:
        model = models.Rol

        fields = ('rol', 'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')