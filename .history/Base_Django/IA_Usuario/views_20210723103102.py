from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from rest_framework import viewsets
from .serializers import *
from .models import *
from .forms import *
from datetime import datetime
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def index(request):
    return render(request,'index.html')

# def sign_up(request):
#     context = {}
#     form = UserCreationForm(request.POST or None)
#     if request.method == "POST":
#         if form.is_valid():
#             user = form.save()
#             login(request,user)
#             return render(request,'usuario/index')
#     context['form']=form
#     return render(request,'registration/sign_up.html',context)

#Area
class AreaViewSet (viewsets.ModelViewSet):
    
    serializer_class = AreaSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_area = Area.objects.all()
        contexto= {
            'areas': lista_area,
        }
        return render(request,'area.html', contexto)

    def crear_Area(request):
        if request.POST:
            form = AreaForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Área creada con éxito...')
                return redirect('../inicio_area/')
        else:
            form = AreaForm()
            contexto = {
                'areaForm' : form,
            }
            return render(request, 'crear_area.html', contexto)

    def editar_area(request, pk):
        post = get_object_or_404(Area, pk=pk)
        if request.method == "POST":
            form = AreaForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                messages.success(request, 'Área editada con éxito...')
                return redirect('../inicio_area/')
        else:
            form = AreaForm(instance=post)
        return render(request, 'crear_area.html', {'areaEditForm': form})

#Dependencia
class DependenciaViewSet (viewsets.ModelViewSet):
    
    serializer_class = DependenciaSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_dependencia = Dependencia.objects.all()
        contexto= {
            'dependencias': lista_dependencia,
        }
        return render(request,'dependencia.html', contexto)

    def crear_dependencia(request):
        if request.POST:
            form = DependenciaForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Dependencia creada con éxito...')
                return redirect('../inicio_dependencia/')
            form = DependenciaForm()
            contexto = {
                'dependenciaForm' : form,
            }
            return render(request, 'crear_dependencia.html', contexto)

    def editar_dependencia(request, pk):
        post = get_object_or_404(Dependencia, pk=pk)
        if request.method == "POST":
            form = DependenciaForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                messages.success(request, 'Dependencia editada con éxito...')
                return redirect('../inicio_dependencia/')
        else:
            form = DependenciaForm(instance=post)
        return render(request, 'crear_dependencia.html', {'dependenciaEditForm': form})

#Cargo
class CargoViewSet (viewsets.ModelViewSet):
    
    serializer_class = CargoSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_cargo = Cargo.objects.all()
        contexto= {
            'cargos': lista_cargo,
        }
        return render(request,'cargo.html', contexto)

    def crear_Cargo(request):
        if request.POST:
            form = CargoForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Cargo creado con éxito...')
                return redirect('../inicio_cargo/')
        else:
            form = CargoForm()
            contexto = {
                'cargoForm' : form,
            }
            return render(request, 'crear_cargo.html', contexto)

    def editar_cargo(request, pk):
        post = get_object_or_404(Cargo, pk=pk)
        if request.method == "POST":
            form = CargoForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                messages.success(request, 'Cargo editado con éxito...')
                return redirect('../inicio_cargo/')
        else:
            form = CargoForm(instance=post)
        return render(request, 'crear_cargo.html', {'cargoEditForm': form})


#Usuario
class UsuarioViewSet (viewsets.ModelViewSet):
    
    serializer_class = UsuarioSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_usuario = Usuario.objects.all()
        contexto= {
            'usuarios': lista_usuario,
        }
        return render(request,'usuario.html', contexto)

    def crear_Usuario(request):
        if request.POST:
            form = UsuarioForm(request.POST)
            cargoform = UsuarioCargoForm(request.POST)
            if form.is_valid:
                form.save()
                cargoform.save()
                messages.success(request, 'Usuario creado con éxito...')
                return redirect('../inicio_usuario/')
        else:
            form = UsuarioForm()
            cargoform = UsuarioCargoForm()
            contexto = {
                'usuarioForm' : form,
                'usuarioCargoForm': cargoform,
            }
            return render(request, 'crear_usuario.html', contexto)

    def editar_usuario(request, pk):
        post = get_object_or_404(Cargo, pk=pk)
        if request.method == "POST":
            form = UsuarioForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                messages.success(request, 'Usuario editado con éxito...')
                return redirect('../inicio_usuario/')
        else:
            form = UsuarioForm(instance=post)
        return render(request, 'crear_usuario.html', {'usuarioEditForm': form})

#Rol
# class RolViewSet (viewsets.ModelViewSet):
    
#     serializer_class = RolSerializer
#     #permission_classes = [] Solicitud de permisos...

#     def redic(request):
#         lista_rol = Rol.objects.all()
#         contexto= {
#             'roles': lista_rol,
#         }
#         return render(request,'rol.html', contexto)

#     def crear_Rol(request):
#         if request.POST:
#             form = RolForm(request.POST)
#             if form.is_valid:
#                 form.save()
#                 messages.success(request, 'Usuario creado con éxito...')
#                 return redirect('../inicio_rol/')
#         else:
#             form = RolForm()
#             contexto = {
#                 'rolForm' : form,
#             }
#             return render(request, 'crear_rol.html', contexto)

#     def editar_rol(request, pk):
#         post = get_object_or_404(Rol, pk=pk)
#         if request.method == "POST":
#             form = RolForm(request.POST, instance=post)
#             if form.is_valid:
#                 form.save()
#                 messages.success(request, 'Usuario editado con éxito...')
#                 return redirect('../inicio_rol/')
#         else:
#             form = RolForm(instance=post)
#         return render(request, 'crear_rol.html', {'rolEditForm': form})
