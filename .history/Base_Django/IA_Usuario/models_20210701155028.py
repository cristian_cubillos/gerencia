from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from datetime import date, datetime
# Create your models here.
numero = 0

class Usuario(AbstractBaseUser):
    email = models.EmailField(verbose_name="email", max_length=60, unique=True, default="user@user.co")
    username = models.CharField(max_length=30, unique=True, default="User " + str(numero + 1))
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=datetime.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=datetime.now())

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.email