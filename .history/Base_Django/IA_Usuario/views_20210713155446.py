from django.shortcuts import render
from rest_framework import viewsets
from .serializers import *
from .models import *
from .forms import *
from datetime import datetime

# Create your views here.

def index(request):
    return render(request, 'index.html')

#Area
class AreaViewSet (viewsets.ModelViewSet):
    
    serializer_class = AreaSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_Area(request):
        if request.POST:
            form = AreaForm(request.POST)
            if form.is_valid:
                area = form.save(commit=False)
                return render(request, 'proyecto.html')
        else:
            form = AreaForm()
            contexto = {
                'areaForm' : form,
            }
            return render(request, 'crear_area.html', contexto)
             

#Dependencia
class DependenciaViewSet (viewsets.ModelViewSet):
    
    serializer_class = DependenciaSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_Dependencia(request):
        if request.POST:
            form = DependenciaForm(request.POST)
            if form.is_valid:
                dependencia = form.save(commit=False)
                return render(request, 'index.html')
        else:            
            form = DependenciaForm()
            contexto = {
                'dependenciaForm' : form,
            }
            return render(request, 'crear_dependencia.html', contexto)

#Cargo
class CargoViewSet (viewsets.ModelViewSet):
    
    serializer_class = CargoSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_Cargo(request):
        if request.POST:
            form = CargoForm(request.POST)
            if form.is_valid:
                cargo = form.save(commit=False)
                return render(request, 'index.html')
        else:
            form = CargoForm()
            contexto = {
                'cargoForm' : form,
            }
            return render(request, 'crear_cargo.html', contexto)


#Usuario
class UsuarioViewSet (viewsets.ModelViewSet):
    
    serializer_class = UsuarioSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_Usuario(request):
        if request.POST:
            form = UsuarioForm(request.POST)
            if form.is_valid:
                usuario = form.save(commit=False)
                return render(request, 'index.html')
        else:
            form = UsuarioForm()
            contexto = {
                'usuarioForm' : form,
            }
            return render(request, 'crear_usuario.html', contexto)

#Rol
class RolViewSet (viewsets.ModelViewSet):
    
    serializer_class = RolSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_Rol(request):
        if request.POST:
            form = RolForm(request.POST)
            if form.is_valid:
                form.save(commit=False)
                contexto = {
                    'message' : "Rol guardado con éxito...",
                }                
                return render(request, 'index.html', contexto)
        else:
            form = RolForm()
            contexto = {
                'rolForm' : form,
            }
            return render(request, 'crear_rol.html', contexto)

