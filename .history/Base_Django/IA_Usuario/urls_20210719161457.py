from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()

urlpatterns = router.urls + [
    path(r'index/', index, name="index"),
    path('crear_area/', AreaViewSet.crear_Area, name="usuario/crear_area"),
    path('crear_dependencia/', DependenciaViewSet.crear_dependencia, name="usuario/crear_dependencia"),
    path('crear_cargo/', CargoViewSet.crear_Cargo, name="usuario/crear_cargo"),
    path('crear_usuario/', UsuarioViewSet.crear_Usuario, name="usuario/crear_usuario"),
    path('crear_rol/', RolViewSet.crear_Rol, name="usuario/crear_rol"),
    path(r'inicio_area/', AreaViewSet.redic, name="usuario/inicio_area"),
    path(r'inicio_dependencia/', DependenciaViewSet.redic, name="proyecto/inicio_dependencia"),
    path(r'inicio_cargo/', CargoViewSet.redic, name="proyecto/inicio_cargo"),
    path(r'inicio_usuario/', UsuarioViewSet.redic, name="proyecto/inicio_usuario"),
    path(r'inicio_rol/', RolViewSet.redic, name="proyecto/inicio_rol"),
    path('editar_area/<int:pk>', AreaViewSet.editar_area, name="proyecto/editar_area"),
    path('editar_dependencia/<int:pk>', DependenciaViewSet.editar_dependencia, name="proyecto/editar_dependencia"),
    path('editar_cargo/<int:pk>', CargoViewSet.editar_cargo, name="proyecto/editar_cargo"),
    path('editar_usuario/<int:pk>', UsuarioViewSet.editar_usuario, name="proyecto/editar_usuario"),
    path('editar_rol/<int:pk>', RolViewSet.editar_rol, name="proyecto/editar_rol"),
]