from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Usuario(User):

    class Meta(User.Meta):
        model = User
        fields= ('username','first_name')

    def __str__(self):
        return self.first_name