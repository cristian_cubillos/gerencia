from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Usuario(User):
    email = models.EmailField(('email address'), unique=True)

    def __str__(self):
        return self.email