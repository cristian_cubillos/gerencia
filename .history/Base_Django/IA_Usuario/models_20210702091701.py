from django.db import models
from django.db.models.base import Model
from django.db.models.enums import Choices
from datetime import datetime
from django.utils import timezone

# Create your models here.

#Usuario
class Usuario(models.Model):
    identificacion = models.CharField(max_length=30, unique=True)
    nombre = models.CharField(max_length=50)
    correo = models.CharField(max_length=30, unique=True)
    contrasenha = models.CharField(max_length=30, unique=True)
    estado = models.IntegerField()
    cargo_ID = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.email


#Área
class Area(models.Model):
    area = models.CharField(max_length=30, unique=True)
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.area

#Dependencia
class Dependencia(models.Model):
    dependencia = models.CharField(max_length=30, unique=True)
    area_ID = models.ForeignKey(Area, on_delete=models.CASCADE)
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.dependencia

#Cargo
class Cargo(models.Model):
    cargo = models.CharField(max_length=30, unique=True)
    dependencia_ID = models.ForeignKey(Dependencia, on_delete=models.CASCADE)
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.dependencia

#Rol 
class Rol(models.Model):
    rol = models.CharField(max_length=30, unique=True)
    usuario_ID = models.ForeignKey(Usuario.identificacion, on_delete=models.CASCADE)
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.rol