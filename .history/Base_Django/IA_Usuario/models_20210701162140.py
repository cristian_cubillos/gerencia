from django.db import models
from django.db.models.base import Model
from django.db.models.enums import Choices
# Create your models here.
numero = 0

class Usuario(models.Model):
    identificacion = models.CharField(max_length=30, unique=True)
    nombre = models.CharField(max_length=50)
    correo = models.CharField(max_length=30, unique=True)
    contrasenha = models.CharField(max_length=30, unique=True)
    estado = models.IntegerField()
    cargo_ID = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=datetime.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=datetime.now())

    def __str__(self):
        return self.email