from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()

urlpatterns = router.urls + [
    path(r'index/', index, name="index"),
    path('crear_area/', AreaViewSet.crear_Area, name="usuario/crear_area"),
    path('crear_dependencia/', DependenciaViewSet.crear_dependencia, name="usuario/crear_dependencia"),
    path('crear_cargo/', CargoViewSet.crear_Cargo, name="usuario/crear_cargo"),
    path('crear_usuario/', UsuarioViewSet.crear_Usuario, name="usuario/crear_usuario"),
    # path('crear_rol/', RolViewSet.crear_Rol, name="usuario/crear_rol"),
    path(r'inicio_area/', AreaViewSet.redic, name="usuario/inicio_area"),
    path(r'inicio_dependencia/', DependenciaViewSet.redic, name="usuario/inicio_dependencia"),
    path(r'inicio_cargo/', CargoViewSet.redic, name="usuario/inicio_cargo"),
    path(r'inicio_usuario/', UsuarioViewSet.redic, name="usuario/inicio_usuario"),
    # path(r'inicio_rol/', RolViewSet.redic, name="usuario/inicio_rol"),
    path('editar_area/<int:pk>', AreaViewSet.editar_area, name="usuario/editar_area"),
    path('editar_dependencia/<int:pk>', DependenciaViewSet.editar_dependencia, name="usuario/editar_dependencia"),
    path('editar_cargo/<int:pk>', CargoViewSet.editar_cargo, name="usuario/editar_cargo"),
    path('editar_usuario/<int:pk>', UsuarioViewSet.editar_usuario, name="usuario/editar_usuario"),
    # path('editar_rol/<int:pk>', RolViewSet.editar_rol, name="usuario/editar_rol"),
]