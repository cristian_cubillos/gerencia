from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()

urlpatterns = router.urls + [
    path(r'index/', index, name="index"),
    path('crear_area/', AreaViewSet.crear_Area, name="usuario/crear_area"),
    path('crear_dependencia/', DependenciaViewSet.crear_Dependencia, name="usuario/crear_dependencia"),
    path('crear_cargo/', CargoViewSet.crear_Cargo, name="usuario/crear_cargo"),
    path('crear_usuario/', UsuarioViewSet.crear_Usuario, name="usuario/crear_usuario"),
    path('crear_rol/', RolViewSet.crear_Rol, name="usuario/crear_rol"),
    path(r'inicio_area/', AreaViewSet.redic, name="usuario/inicio_area"),
    # path(r'inicio_elemento/', ElementoFlujoViewSet.redic, name="proyecto/inicio_flujo"),
    # path(r'inicio_historial/', ProyectoViewSet.redic, name="proyecto/inicio_Proyecto"),
    # path(r'inicio_requerimiento/', ElementoFlujoViewSet.redic, name="proyecto/inicio_flujo"),
    # path(r'inicio_tarea/', ProyectoViewSet.redic, name="proyecto/inicio_Proyecto"),    
]



    # path(r'inicio_proyecto/', ProyectoViewSet.redic, name="proyecto/inicio_Proyecto"),
    # path(r'inicio_elemento/', ElementoFlujoViewSet.redic, name="proyecto/inicio_flujo"),
    # path(r'inicio_historial/', ProyectoViewSet.redic, name="proyecto/inicio_Proyecto"),
    # path(r'inicio_requerimiento/', ElementoFlujoViewSet.redic, name="proyecto/inicio_flujo"),
    # path(r'inicio_tarea/', ProyectoViewSet.redic, name="proyecto/inicio_Proyecto"),
    # path('crear_tarea/', TareaViewSet.crear_tarea, name="proyecto/crear_tarea"),
    # path('crear_proyecto/', ProyectoViewSet.crear_proyecto, name="proyecto/crear_proyecto"),
    # path('crear_historial_componente/', HistorialComponenteViewSet.crear_historial, name="proyecto/crear_historial_componente"),
    # path('crear_requerimiento/', RequerimientoViewSet.crear_requerimiento, name="proyecto/crear_requerimiento"),
    # path('crear_elemento/', ElementoFlujoViewSet.crear_elemento, name="proyecto/crear_elemento"),
    # path('editar_proyecto/<int:pk>', ProyectoViewSet.editar_proyecto, name="proyecto/editar_proyecto"),
    # path('editar_elemento/<int:pk>', ProyectoViewSet.editar_proyecto, name="proyecto/editar_elemento"),
    # path('editar_historial/<int:pk>', ProyectoViewSet.editar_proyecto, name="proyecto/editar_historial"),
    # path('editar_requerimiento/<int:pk>', ProyectoViewSet.editar_proyecto, name="proyecto/editar_requerimiento"),
    # path('editar_tarea/<int:pk>', ProyectoViewSet.editar_proyecto, name="proyecto/editar_tarea"),
