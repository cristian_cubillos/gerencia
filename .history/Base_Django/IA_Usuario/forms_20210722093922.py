from django import forms
from django.forms import fields
from django.forms.widgets import Textarea
from .models import *

#Area
class AreaForm(forms.ModelForm):
    # titulo = forms.CharField(max_length=50, label="Titulo")
    # responsable = forms.CharField(max_length=50, label="Responsable")
    # cliente = forms.CharField(max_length=50, label="Cliente")
    # objetivo = forms.CharField(widget=forms.Textarea(attrs={ 'rows': 5 }),label="Objetivo")
    # descripcion = forms.CharField(widget=forms.Textarea(attrs={ 'rows': 5 }),label="Descripción")
    # estado = forms.ChoiceField(widget=forms.RadioSelect, 
    #     choices=[
    #         (1, 'En proceso'),
    #         (2, 'En espera'),
    #         (3, 'Aprobado'),
    #         (4, 'Archivado'),
    #     ],label="Estado")

    class Meta:
        model = Area
        fields = ('area', 'estado')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
                                choices=[
                                    (1, 'Activo'),
                                    (2, 'Inactivo'),
                                ],label="Estado")


#Dependencia
class DependenciaForm(forms.ModelForm):
    # requeimiento = forms.CharField(widget=forms.Textarea(attrs={ 'size': 50 }),label="Requerimiento")
    # proyecto_ID = forms.CharField(max_length=30, label="Código proyecto")
    # estado = forms.IntegerField(label="Estado")

    class Meta:
        model = Dependencia
        fields = ('dependencia', 'area_ID', 'estado')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['area_ID'].queryset = Area.objects.all()
        self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
                                choices=[
                                    (1, 'Activo'),
                                    (2, 'Inactivo'),
                                ],label="Estado")
        

#Cargo
class CargoForm(forms.ModelForm):
    # flujo_ID = forms.CharField(max_length=30, label="Código WorkFlow")
    # proyecto_ID = forms.CharField(max_length=30, label="Código Proyecto")
    # responsable = forms.CharField(max_length=30, label="Responsable")
    # fecha_Inicio = forms.DateTimeField(label="Fecha inicio")
    # fecha_Vencimiento = forms.DateTimeField(label="Fecha vencimiento")
    # estado = forms.IntegerField(label="Estado")

    class Meta:
        model = Cargo
        fields = ('cargo', 'dependencia_ID', 'estado')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['dependencia_ID'].queryset = Dependencia.objects.all()
        self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
                                choices=[
                                    (1, 'Activo'),
                                    (2, 'Inactivo'),
                                ],label="Estado")

#Usuario
class UsuarioForm(forms.ModelForm):
    # Componente_ID = forms.CharField(max_length=30, label="Código componente")
    # elemento_Flujo_ID = forms.CharField(max_length=30, label="Código elemento del flujo")
    # fecha_Elemento = forms.DateTimeField(label="Fecha del elemento")
    # estado = forms.IntegerField(label="Estado")

    class Meta:
        model = User
        fields = ('password', 'is_superuser', 'username',
            'first_name', 'last_name', 'email',
            'is_staff', 'is_active',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'] = forms.EmailField(label="Correo Electronico")
        self.fields['password'] = forms.CharField(widget=forms.PasswordInput)
        #self.fields['cargo_ID'].queryset = Cargo.objects.all()
        #self.fields['rol_ID'].queryset = Rol.objects.all()
        # self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
        #                         choices=[
        #                             (1, 'Activo'),
        #                             (2, 'Inactivo'),
        #                         ],label="Estado")

class UsuarioCargoForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('cargo_ID',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['cargo_ID'].queryset = Cargo.objects.all()


#Rol
# class RolForm(forms.ModelForm):
#     # nombre = forms.CharField(max_length=30, label="Titulo")
#     # descripcion = forms.CharField(widget=forms.Textarea,label="Descripción")
#     # requerido = forms.BooleanField(label="Tarea requerida")
#     # historial_Componente_ID = forms.CharField(max_length=30, label="Codigo componente contenedor")
#     # responsable = forms.CharField(max_length=30, label="Responsable tarea")
#     # fecha_Inicio = forms.DateTimeField(label="Fecha inicio")
#     # duracion = forms.IntegerField(label="Duración (Tiempo aprox. en días)") #Tiempo en Días
#     # actividad_ID = forms.CharField(max_length=30, label="Código actividad")
#     # estado = forms.IntegerField(label="Estado")

#     class Meta:
#         model = Rol
#         fields = ('rol', 'estado')

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
#                                 choices=[
#                                     (1, 'Activo'),
#                                     (2, 'Inactivo'),
#                                 ],label="Estado")

class UsuarioLoginForm(forms.ModelForm):
    # Componente_ID = forms.CharField(max_length=30, label="Código componente")
    # elemento_Flujo_ID = forms.CharField(max_length=30, label="Código elemento del flujo")
    # fecha_Elemento = forms.DateTimeField(label="Fecha del elemento")
    # estado = forms.IntegerField(label="Estado")

    class Meta:
        model = User
        fields = ('username', 'password')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'] = forms.CharField(max_length=50)
        self.fields['password'] = forms.CharField(widget=forms.PasswordInput)