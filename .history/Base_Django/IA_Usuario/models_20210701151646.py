from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

# Create your models here.

class Usuario(AbstractBaseUser):
    email = models.EmailField(verbose_name="email", max_length=60, unique=True)
    username = models.CharField(max_length=30, unique=True)
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion",auto_now_add=True)
    fecha_Update = models.DateField(verbose_name="fecha_Update", auto_now=True)

    def __str__(self):
        return self.email