from django.shortcuts import get_object_or_404, render, redirect
from django.contrib import messages
from rest_framework import viewsets
from .serializers import *
from .models import *
from .forms import *
from datetime import datetime

# Create your views here.

def index(request):
    return render(request, 'index.html')

#Area
class AreaViewSet (viewsets.ModelViewSet):
    
    serializer_class = AreaSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_area = Area.objects.all()
        contexto= {
            'areas': lista_area,
        }
        return render(request,'area.html', contexto)

    def crear_Area(request):
        if request.POST:
            form = AreaForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Área creada con éxito...')
                return redirect('../inicio_area/')
        else:
            form = AreaForm()
            contexto = {
                'areaForm' : form,
            }
            return render(request, 'crear_area.html', contexto)

    def editar_area(request, pk):
        post = get_object_or_404(Area, pk=pk)
        if request.method == "POST":
            form = AreaForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                messages.success(request, 'Área editada con éxito...')
                return redirect('../inicio_area/')
        else:
            form = AreaForm(instance=post)
        return render(request, 'crear_area.html', {'areaEditForm': form})

#Dependencia
class DependenciaViewSet (viewsets.ModelViewSet):
    
    serializer_class = DependenciaSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_Dependencia(request):
        if request.POST:
            form = DependenciaForm(request.POST)
            if form.is_valid:
                form.save()

                contexto= {
                    'message': "Dependencia guardada con éxito...",
                }

                return render(request, 'index.html', contexto)
        else:            
            form = DependenciaForm()
            contexto = {
                'dependenciaForm' : form,
            }
            return render(request, 'crear_dependencia.html', contexto)

#Cargo
class CargoViewSet (viewsets.ModelViewSet):
    
    serializer_class = CargoSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_Cargo(request):
        if request.POST:
            form = CargoForm(request.POST)
            if form.is_valid:
                form.save()

                contexto= {
                    'message': "Cargo guardado con éxito...",
                }

                return render(request, 'index.html', contexto)
        else:
            form = CargoForm()
            contexto = {
                'cargoForm' : form,
            }
            return render(request, 'crear_cargo.html', contexto)


#Usuario
class UsuarioViewSet (viewsets.ModelViewSet):
    
    serializer_class = UsuarioSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_Usuario(request):
        if request.POST:
            form = UsuarioForm(request.POST)
            if form.is_valid:
                form.save()

                contexto= {
                    'message': "Usuario guardado con éxito...",
                }

                return render(request, 'index.html', contexto)
        else:
            form = UsuarioForm()
            contexto = {
                'usuarioForm' : form,
            }
            return render(request, 'crear_usuario.html', contexto)

#Rol
class RolViewSet (viewsets.ModelViewSet):
    
    serializer_class = RolSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_Rol(request):
        if request.POST:
            form = RolForm(request.POST)
            if form.is_valid:
                form.save()
                contexto = {
                    'message' : "Rol guardado con éxito...",
                }                
                return render(request, 'index.html', contexto)
        else:
            form = RolForm()
            contexto = {
                'rolForm' : form,
            }
            return render(request, 'crear_rol.html', contexto)

