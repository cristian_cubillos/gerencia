from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()

urlpatterns = router.urls + [
    # path('monitor/crear_monitoreo', GrupoMonitoreoIAViewSet.as_view({'post':'crear'})),
    # path('monitor/guardar_monitoreo', MonitoreoIAViewSet.as_view({'post':'emociones'})),
    # path('monitor/porcentajes', MonitoreoIAViewSet.as_view({'post':'porcentajes'})),
]
