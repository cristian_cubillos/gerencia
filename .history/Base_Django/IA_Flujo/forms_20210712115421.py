from django import forms

#Flujo 
class Flujo(forms.Form):
    tiempo_promedio = forms.IntegerField(label="Tiempo estimado (Días)") #Tiempo en Días
    estado = forms.IntegerField(label="Estado")

#Componente
class Componente(forms.Form):
    titulo = forms.CharField(max_length=50, label="Titulo")
    descripcion = forms.TextField(label="Descripción")
    flujo_ID = forms.CharField(max_length=50, label="Código WorkFlow")
    fecha_Inicio = forms.DateTimeField(label="Fecha inicio")
    fecha_Vencimiento = forms.DateTimeField(label="Fecha vencimiento")
    estado = forms.IntegerField(label="Estado")

#Reglas
class Regla(forms.Form):
    componente_Inicial = forms.CharField(max_length=50, label="Viene del proceso")
    componente_Final = forms.CharField(max_length=50, label="Puede pasar al proceso")
    estado = forms.IntegerField(label="Estado")

#Actividad
class Actividad(forms.Form):
    titulo = forms.CharField(max_length=50, label="Titulo")
    descripcion = forms.TextField(label="Descripción")
    entregable = forms.CharField(max_length=30, label="Entregable")
    requerido = forms.BooleanField(label="Actividad requerida")
    fecha_Inicio = forms.DateField(label="Fecha inicio")
    fecha_Final = forms.DateField(label="Fecha final")
    estado = forms.IntegerField(label="Estado")