from rest_framework import serializers
from IA_Flujo.models import Actividad, Componente, Flujo, Regla

#Serializadores...

#Serializador Actividad
class ActividadSerializer(serializers.Serializer):
    class Meta:
        model = Actividad

        fields = ('titulo', 'descripcion', 'entregable', 'requerido'
            'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializador Componente
class ComponenteSerializer(serializers.Serializer):
    class Meta:
        model = Componente

        fields = ('flujo_ID', 'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializador Flujo
class FlujoSerializer(serializers.Serializer):
    class Meta:
        model = Flujo

        fields = ('titulo', 'descripción', 'tiempo_Promedio', 
            'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializador Regla
class ReglaSerializer(serializers.Serializer):
    class Meta:
        model = Regla

        fields = ('componente_Inicial', 'componente_Final', 'validaciones',
            'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')
