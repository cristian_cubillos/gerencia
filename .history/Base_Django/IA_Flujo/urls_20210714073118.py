from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()

urlpatterns = router.urls + [
    path(r'inicio_Flujo/', FlujoViewSet.redic, name="flujo/inicio_Flujo"),
    path(r'crear_flujo/', FlujoViewSet.crear_flujo, name="flujo/crear_flujo"),
    # path('monitor/porcentajes', MonitoreoIAViewSet.as_view({'post':'porcentajes'})),
]
