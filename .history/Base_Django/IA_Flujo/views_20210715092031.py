from django.http import response
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.decorators import action
from .serializers import *
from .forms import *

# Create your views here.

#Flujo
class FlujoViewSet (viewsets.ModelViewSet):
    
    serializer_class = FlujoSerializer
    #permission_classes = [] Solicitud de permisos...

    # @action(detail=True, methods=['get'])
    def crear_flujo(request):
        if request.POST:
            form = FlujoForm(request.POST)
            if form.is_valid:
                form.save()
                return render(request, 'index.html')
        else:
            form = FlujoForm()
            contexto = {
                'flujoForm' : form,
            }
            return render(request, 'crear_flujo.html', contexto)

#Componente
class ComponenteViewSet (viewsets.ModelViewSet):
    
    serializer_class = ComponenteSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_componente(request):
        if request.POST:
            form = ComponenteForm(request.POST)
            if form.is_valid:
                form.save()
                return render(request, 'index.html')
        else:
            form = ComponenteForm()

            print (form)
            contexto = {
                'componenteForm' : form,
            }
            return render(request, 'crear_componente.html')

#Actividad
class ActividadViewSet (viewsets.ModelViewSet):
    
    serializer_class = ActividadSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_actividad(request):
        if request.POST:
            form = ActividadForm(request.POST)
            if form.is_valid:
                form.save()
                return render(request, 'index.html')
        else:
            form = ActividadForm()
            contexto = {
                'actividadForm' : form,
            }
            return render(request, 'crear_actividad.html', contexto)

#Regla
class ReglaViewSet (viewsets.ModelViewSet):
    
    serializer_class = ReglaSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_regla(request):
        if request.POST:
            form = ReglaForm(request.POST)
            if form.is_valid:
                form.save()
                return render(request, 'index.html')
        else:
            form = ReglaForm()
            contexto = {
                'reglaForm' : form,
            }
            return render(request, 'crear_regla.html', contexto)
