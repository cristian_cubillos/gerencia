from django.http import response
from django.shortcuts import get_object_or_404, redirect, render
from rest_framework import viewsets
from rest_framework.decorators import action
from .serializers import *
from .forms import *

# Create your views here.

#Flujo
class FlujoViewSet (viewsets.ModelViewSet):
    
    serializer_class = FlujoSerializer
    #permission_classes = [] Solicitud de permisos...

    # @action(detail=True, methods=['get'])

    def redic(request):
        lista_flujo = Flujo.objects.all()
        contexto= {
            'procesos': lista_flujo,
        }
        return render(request,'flujo/inicio_flujo', contexto)        

    def crear_flujo(request):
        if request.POST:
            form = FlujoForm(request.POST)
            if form.is_valid:
                form.save()
                contexto={
                    'message' : "Flujo de Trabajo guardado con éxito...",
                }
                return redirect('flujo/inicio_flujo', contexto)
        else:
            form = FlujoForm()
            contexto = {
                'flujoForm' : form,
            }
            return render(request, 'crear_flujo.html', contexto)

    def editar_flujo(request, pk):
        post = get_object_or_404(Flujo, pk=pk)
        if request.method == "POST":
            form = FlujoForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                lista_flujo = Flujo.objects.all()
                contexto={
                    'message' : "Flujo de Trabajo guardado con éxito...",
                    'flujos' : lista_flujo,
                }
                return redirect('flujo/inicio_flujo', contexto)
        else:
            form = FlujoForm(instance=post)
        return render(request, 'crear_flujo.html', {'flujoFormEdit': form})


#Componente
class ComponenteViewSet (viewsets.ModelViewSet):
    
    serializer_class = ComponenteSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_componentes = Componente.objects.all()
        contexto= {
            'componentes': lista_componentes,
        }
        return render(request,'flujo_componente.html', contexto)      

    def crear_componente(request):
        if request.POST:
            form = ComponenteForm(request.POST)
            if form.is_valid:
                form.save()
                contexto={
                    'message' : "Componente guardado con éxito...",
                }
                return redirect('flujo/inicio_componente', contexto)
        else:
            form = ComponenteForm()
            contexto = {
                'componenteForm' : form,
            }
            return render(request, 'crear_componente.html', contexto)

    def editar_componente(request, pk):
        post = get_object_or_404(Componente, pk=pk)
        if request.method == "POST":
            form = ComponenteForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                lista_componentes = Componente.objects.all()
                contexto={
                    'message' : "Componente editado con éxito...",
                    'componentes' : lista_componentes,
                }
                return redirect('flujo/inicio_flujo', contexto)
        else:
            form = ComponenteForm(instance=post)
        return render(request, 'crear_componente.html', {'componenteFormEdit': form})


#Actividad
class ActividadViewSet (viewsets.ModelViewSet):
    
    serializer_class = ActividadSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_tarea = Actividad.objects.all()
        contexto= {
            'tareas': lista_tarea,
        }
        return render(request,'flujo_actividades.html', contexto)     

    def crear_actividad(request):
        if request.POST:
            form = ActividadForm(request.POST)
            if form.is_valid:
                form.save()
                contexto={
                    'message' : "Actividad guardada con éxito...",
                }
                return redirect('flujo/inicio_actividad', contexto)
        else:
            form = ActividadForm()
            contexto = {
                'actividadForm' : form,
            }
            return render(request, 'crear_actividad.html', contexto)

    def editar_actividad(request, pk):
        post = get_object_or_404(Actividad, pk=pk)
        if request.method == "POST":
            form = ActividadForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                lista_actividad = Actividad.objects.all()
                contexto={
                    'message' : "Actividad editada con éxito...",
                    'actividades' : lista_actividad,
                }
                return redirect('flujo/inicio_flujo', contexto)
        else:
            form = ActividadForm(instance=post)
        return render(request, 'crear_actividad.html', {'actividadFormEdit': form})

#Regla
class ReglaViewSet (viewsets.ModelViewSet):
    
    serializer_class = ReglaSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_regla = Regla.objects.all()
        contexto= {
            'reglas': lista_regla,
        }
        return render(request,'flujo_regla.html', contexto)     

    def crear_regla(request):
        if request.POST:
            form = ReglaForm(request.POST)
            if form.is_valid:
                form.save()
                contexto={
                    'message' : "Regla guardada con éxito...",
                }
                return redirect('flujo/inicio_regla', contexto)
        else:
            form = ReglaForm()
            contexto = {
                'reglaForm' : form,
            }
            return render(request, 'crear_regla.html', contexto)

    def editar_regla(request, pk):
        post = get_object_or_404(Regla, pk=pk)
        if request.method == "POST":
            form = ReglaForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                lista_regla = Regla.objects.all()
                contexto={
                    'message' : "Regla Editada con éxito...",
                    'reglas' : lista_regla,
                }
                return redirect('flujo/inicio_flujo', contexto)
        else:
            form = ReglaForm(instance=post)
        return render(request, 'crear_regla.html', {'reglaFormEdit': form})

