from django.http import response
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.decorators import action
from .serializers import *

# Create your views here.

#Flujo
class FlujoViewSet (viewsets.ModelViewSet):
    
    serializer_class = FlujoSerializer
    #permission_classes = [] Solicitud de permisos...

    # @action(detail=True, methods=['get'])
    def redic(request):
        return render(request, 'flujo.html') 

#Componente
class ComponenteViewSet (viewsets.ModelViewSet):
    
    serializer_class = ComponenteSerializer
    #permission_classes = [] Solicitud de permisos...

#Actividad
class ActividadViewSet (viewsets.ModelViewSet):
    
    serializer_class = ActividadSerializer
    #permission_classes = [] Solicitud de permisos...

#Regla
class ReglaViewSet (viewsets.ModelViewSet):
    
    serializer_class = ReglaSerializer
    #permission_classes = [] Solicitud de permisos...
