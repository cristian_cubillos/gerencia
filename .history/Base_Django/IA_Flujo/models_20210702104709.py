from django.db import models
from django.db.models.base import Model
from django.utils import timezone

# Create your models here.

#Flujo 
class Flujo(models.Model):
    tiempo_promedio = models.IntegerField() #Tiempo en Días
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.id

#Componente
class Componente(models.Model):
    titulo = models.CharField(max_length=50, unique=True)
    descripcion = models.TextField()
    flujo_ID = models.ForeignKey(Flujo, on_delete=models.CASCADE)
    fecha_Inicio = models.DateTimeField()
    fecha_Vencimiento = models.DateTimeField()
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.titulo

#Reglas
class Regla(models.Model):
    componente_Inicial = models.OneToOneField(Componente, on_delete=models.CASCADE)
    componente_Final = models.OneToOneField(Componente, on_delete=models.CASCADE)
    validaciones = models.JSONField()
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.id

#Actividad
class Actividad(models.Model):
    titulo = models.CharField(max_length=50, unique=True)
    descripcion = models.TextField()
    entregable = models.CharField(max_length=30, unique=True)
    requerido = models.BooleanField()
    fecha_Inicio = models.DateField()
    fecha_Final = models.DateField()
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.titulo
