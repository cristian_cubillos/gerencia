from rest_framework import serializers
from IA_Flujo.models import Actividad, Componente, Flujo, Regla

#Serializadores...

#Serializador Actividad
class ActividadSerializer(serializers.Serializer):
    class Meta:
        model = Actividad

        fields = ('titulo', 'descripcion', 'entregable', 'requerido'
            'fecha_Inicio', 'fecha_Final', 'estado', 'fecha_Creacion',
            'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializador Componente
class ComponenteSerializer(serializers.Serializer):
    class Meta:
        model = Componente

        fields = ('titulo', 'descripcion', 'flujo_ID', 'fecha_Inicio', 
            'fecha_Vencimiento', 'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializador Flujo
class FlujoSerializer(serializers.Serializer):
    class Meta:
        model = Flujo

        fields = ('tiempo_Promedio', 'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')

#Serializador Regla
class ReglaSerializer(serializers.Serializer):
    class Meta:
        model = Regla

        fields = ('componente_Inicial', 'componente_Final', 'validaciones',
            'estado', 'fecha_Creacion', 'fecha_Update')
        
        read_only_fields = ('fecha_Creacion', 'fecha_Update')
