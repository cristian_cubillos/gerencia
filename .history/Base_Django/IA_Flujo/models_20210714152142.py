from django.db import models
from django.utils import timezone

# Create your models here.

#Flujo 
class Flujo(models.Model):
    titulo = models.CharField(max_length=100, unique=True)
    descripcion = models.TextField()
    tiempo_promedio = models.IntegerField() #Tiempo en Días
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.id

#Componente
class Componente(models.Model):
    titulo = models.CharField(max_length=100, unique=True)
    descripcion = models.TextField()
    flujo_ID = models.ForeignKey(Flujo, on_delete=models.CASCADE)
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.pk

#Reglas
class Regla(models.Model):
    componente_Inicial = models.ForeignKey(Componente, on_delete=models.CASCADE,
        related_name = 'componente_Inicial',verbose_name = "componente_Inicial")
    componente_Final = models.ForeignKey(Componente, on_delete=models.CASCADE,
        related_name = 'componente_Final',verbose_name = "componente_Final")
    validaciones = models.JSONField()
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.id

#Actividad
class Actividad(models.Model):
    titulo = models.CharField(max_length=100, unique=True)
    descripcion = models.TextField()
    entregable = models.CharField(max_length=100, unique=True)
    requerido = models.BooleanField()
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.titulo
