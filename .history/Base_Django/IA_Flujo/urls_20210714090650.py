from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()

urlpatterns = router.urls + [
    path(r'inicio_Flujo/', FlujoViewSet.redic, name="flujo/inicio_Flujo"),
    path(r'crear_flujo/', FlujoViewSet.crear_flujo, name="flujo/crear_flujo"),
    path(r'crear_componente/', ComponenteViewSet.crear_componente, name="flujo/crear_componente"),
    path(r'crear_actividad/', ActividadViewSet.crear_actividad, name="flujo/crear_actividad"),
    path(r'crear_regla/', ReglaViewSet.crear_regla, name="flujo/crear_regla"),
]
