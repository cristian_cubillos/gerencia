from django.contrib.auth.decorators import login_required
from django.http import response, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib import messages
from rest_framework import viewsets
from rest_framework.decorators import action
from .serializers import *
from .forms import *

# Create your views here.

#Flujo
class FlujoViewSet (viewsets.ModelViewSet):
    
    serializer_class = FlujoSerializer
    #permission_classes = [] Solicitud de permisos...

    # @action(detail=True, methods=['get'])

    @login_required
    def redic(request):
        lista_flujo = Flujo.objects.all()
        contexto= {
            'procesos': lista_flujo,
        }
        return render(request,'flujo_flujo.html', contexto)        

    @login_required
    def crear_flujo(request):
        if request.POST:
            form = FlujoForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Flujo Creado con éxito...')
                return redirect('../inicio_flujo/')
        else:
            form = FlujoForm()
            contexto = {
                'flujoForm' : form,
            }
            return render(request, 'crear_flujo.html', contexto)

    @login_required
    def editar_flujo(request, pk):
        post = get_object_or_404(Flujo, pk=pk)
        if request.method == "POST":
            form = FlujoForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                lista_flujo = Flujo.objects.all()
                messages.success(request, 'Flujo editado con éxito...')
                return redirect('../inicio_flujo/')
        else:
            form = FlujoForm(instance=post)
        return render(request, 'crear_flujo.html', {'flujoFormEdit': form})


#Componente
class ComponenteViewSet (viewsets.ModelViewSet):
    
    serializer_class = ComponenteSerializer
    #permission_classes = [] Solicitud de permisos...

    @login_required
    def redic(request):
        lista_componentes = Componente.objects.all()
        contexto= {
            'componentes': lista_componentes,
        }
        return render(request,'flujo_componente.html', contexto)      

    @login_required
    def crear_componente(request):
        if request.POST:
            form = ComponenteForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Componente creado con éxito...')
                return redirect('../inicio_componente/')
        else:
            form = ComponenteForm()
            contexto = {
                'componenteForm' : form,
            }
            return render(request, 'crear_componente.html', contexto)

    @login_required
    def editar_componente(request, pk):
        post = get_object_or_404(Componente, pk=pk)
        if request.method == "POST":
            form = ComponenteForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                messages.success(request, 'Componente editado con éxito...')
                return redirect('../inicio_componente/')
        else:
            form = ComponenteForm(instance=post)
        return render(request, 'crear_componente.html', {'componenteFormEdit': form})


#Actividad
class ActividadViewSet (viewsets.ModelViewSet):
    
    serializer_class = ActividadSerializer
    #permission_classes = [] Solicitud de permisos...

    @login_required
    def redic(request):
        lista_tarea = Actividad.objects.all()
        contexto= {
            'tareas': lista_tarea,
        }
        return render(request,'flujo_actividades.html', contexto)     

    @login_required
    def crear_actividad(request):
        if request.POST:
            form = ActividadForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Actividad creada con éxito...')
                return redirect('../inicio_actividad/')
        else:
            form = ActividadForm()
            contexto = {
                'actividadForm' : form,
            }
            return render(request, 'crear_actividad.html', contexto)

    @login_required
    def editar_actividad(request, pk):
        post = get_object_or_404(Actividad, pk=pk)
        if request.method == "POST":
            form = ActividadForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                lista_actividad = Actividad.objects.all()
                messages.success(request, 'Actividad editado con éxito...')
                return redirect('../inicio_actividad/')
        else:
            form = ActividadForm(instance=post)
        return render(request, 'crear_actividad.html', {'actividadFormEdit': form})

#Regla
class ReglaViewSet (viewsets.ModelViewSet):
    
    serializer_class = ReglaSerializer
    #permission_classes = [] Solicitud de permisos...

    @login_required
    def redic(request):
        lista_regla = Regla.objects.all()
        contexto= {
            'reglas': lista_regla,
        }
        return render(request,'flujo_regla.html', contexto)     

    @login_required
    def crear_regla(request):
        if request.POST:
            form = ReglaForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Regla creada con éxito...')
                return redirect('../inicio_regla/')
        else:
            form = ReglaForm()
            contexto = {
                'reglaForm' : form,
            }
            return render(request, 'crear_regla.html', contexto)

    @login_required
    def editar_regla(request, pk):
        post = get_object_or_404(Regla, pk=pk)
        if request.method == "POST":
            form = ReglaForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                lista_regla = Regla.objects.all()
                messages.success(request, 'Regla editada con éxito...')
                return redirect('../inicio_regla/')
        else:
            form = ReglaForm(instance=post)
        return render(request, 'crear_regla.html', {'reglaFormEdit': form})