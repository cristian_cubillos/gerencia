from django import forms
from .models import *


#Flujo 
class FlujoForm(forms.ModelForm):
    # tiempo_promedio = forms.IntegerField(label="Tiempo estimado (Días)") #Tiempo en Días
    # estado = forms.IntegerField(label="Estado")

    class Meta:
        model = Flujo
        fields = ('titulo','descripcion',
            'tiempo_promedio', 'estado')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['tiempo_promedio'] = forms.IntegerField(label="Tiempo Promedio (En Días)")
        self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
                                choices=[
                                    (1, 'Activo'),
                                    (2, 'Inactivo'),
                                ],label="Estado")

Componente
class ComponenteForm(forms.ModelForm):
    # titulo = forms.CharField(max_length=50, label="Titulo")
    # descripcion = forms.CharField(widget=forms.Textarea,label="Descripción")
    # flujo_ID = forms.CharField(max_length=50, label="Código WorkFlow")
    # fecha_Inicio = forms.DateTimeField(label="Fecha inicio")
    # fecha_Vencimiento = forms.DateTimeField(label="Fecha vencimiento")
    # estado = forms.IntegerField(label="Estado")

    class Meta:
        model = Componente
        fields = ('flujo_ID', 'estado')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['flujo_ID'].queryset = Flujo.objects.all()
        self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
                                choices=[
                                    (1, 'Activo'),
                                    (2, 'Inactivo'),
                                ],label="Estado")

class ReglaForm(forms.ModelForm):
    # componente_Inicial = forms.CharField(max_length=50, label="Viene del proceso")
    # componente_Final = forms.CharField(max_length=50, label="Puede pasar al proceso")
    # estado = forms.IntegerField(label="Estado")

    class Meta:
        model = Regla
        # fields = ('componente_Inicial', 'componente_Final',
        #     'validaciones', 'estado')
        fields = ('validaciones', 'estado')        

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #self.fields['componente_Inicial'].queryset = Componente.objects.all()
        #self.fields['componente_Final'].queryset = Componente.objects.all()
        self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
                                choices=[
                                    (1, 'Activo'),
                                    (2, 'Inactivo'),
                                ],label="Estado")

#Actividad
class ActividadForm(forms.ModelForm):
    # titulo = forms.CharField(max_length=50, label="Titulo")
    # descripcion = forms.TextField(label="Descripción")
    # entregable = forms.CharField(max_length=30, label="Entregable")
    # requerido = forms.BooleanField(label="Actividad requerida")
    # fecha_Inicio = forms.DateField(label="Fecha inicio")
    # fecha_Final = forms.DateField(label="Fecha final")
    # estado = forms.IntegerField(label="Estado")

    class Meta:
        model = Actividad
        fields = ('titulo', 'descripcion', 'entregable',
            'requerido', 'estado')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
                                choices=[
                                    (1, 'Activo'),
                                    (2, 'Inactivo'),
                                ],label="Estado")
