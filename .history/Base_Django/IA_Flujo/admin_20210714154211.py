from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Flujo)
admin.site.register(Componentes)
admin.site.register(Regla)
admin.site.register(Actividad)

