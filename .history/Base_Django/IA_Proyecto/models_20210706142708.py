from django.db import models
from django.db.models.base import Model
from IA_Usuario.models import Usuario
from IA_Flujo.models import Flujo, Componente
from django.utils import timezone

# Create your models here.

#Proyecto
class Proyecto(models.Model):
    titulo = models.CharField(max_length=50, unique=True)
    responsable = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    cliente = models.CharField(max_length=50, unique=True)
    objetivo = models.TextField()
    descripcion = models.TextField()
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.titulo

#Requerimientos del proyecto
class Requerimientos(models.Model):
    requeimiento = models.TextField()
    proyecto_ID = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.requeimiento

#Elementos del Flujo
class Elemento_Flujo(models.Model):
    flujo_ID = models.ForeignKey(Flujo, on_delete=models.CASCADE)
    proyecto_ID = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    responsable = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    fecha_Inicio = models.DateTimeField()
    fecha_Vencimiento = models.DateTimeField()
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.id

#Historial_Componente
class Historial_Componente(models.Model):
    Componente_ID = models.ForeignKey(Componente, on_delete=models.CASCADE)
    elemento_Flujo_ID = models.ForeignKey(Elemento_Flujo,on_delete=models.CASCADE)
    fecha_Elemento = models.DateTimeField()
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.id

#Tarea
class Tarea(models.Model):
    nombre = models.CharField(max_length=30, unique=True)
    descripcion = models.TextField()
    requerido = models.BooleanField()
    historial_Componente_ID = models.ForeignKey(Historial_Componente, on_delete=models.CASCADE)
    responsable = models.ForeignKey(Usuario,on_delete=models.CASCADE)
    fecha_Inicio = models.DateTimeField()
    duracion = models.IntegerField() #Tiempo en Días
    estado = models.IntegerField()
    fecha_Creacion = models.DateField(verbose_name="fecha_Creacion", default=timezone.now())
    fecha_Update = models.DateField(verbose_name="fecha_Update", default=timezone.now())

    def __str__(self):
        return self.id
