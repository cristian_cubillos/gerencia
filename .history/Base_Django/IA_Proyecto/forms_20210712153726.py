from django import forms
from django.forms.widgets import Textarea

#Proyecto
class ProyectoForm(forms.Form):
    titulo = forms.CharField(max_length=50, label="Titulo")
    responsable = forms.CharField(max_length=50, label="Responsable")
    cliente = forms.CharField(max_length=50, label="Cliente")
    objetivo = forms.CharField(widget=forms.Textarea(attrs={ 'size': 50 }),label="Objetivo")
    descripcion = forms.CharField(widget=forms.Textarea(attrs={ 'size': 50 }),label="Descripción")
    estado = forms.IntegerField(label="Estado")

#Requerimientos del proyecto
class RequerimientosForm(forms.Form):
    requeimiento = forms.CharField(widget=forms.Textarea(attrs={ 'size': 50 }),label="Requerimiento")
    proyecto_ID = forms.CharField(max_length=30, label="Código proyecto")
    estado = forms.IntegerField(label="Estado")

#Elementos del Flujo
class Elemento_FlujoForm(forms.Form):
    flujo_ID = forms.CharField(max_length=30, label="Código WorkFlow")
    proyecto_ID = forms.CharField(max_length=30, label="Código Proyecto")
    responsable = forms.CharField(max_length=30, label="Responsable")
    fecha_Inicio = forms.DateTimeField(label="Fecha inicio")
    fecha_Vencimiento = forms.DateTimeField(label="Fecha vencimiento")
    estado = forms.IntegerField(label="Estado")


#Historial_Componente
class Historial_ComponenteForm(forms.Form):
    Componente_ID = forms.CharField(max_length=30, label="Código componente")
    elemento_Flujo_ID = forms.CharField(max_length=30, label="Código elemento del flujo")
    fecha_Elemento = forms.DateTimeField(label="Fecha del elemento")
    estado = forms.IntegerField(label="Estado")

#Tarea
class TareaForm(forms.Form):
    nombre = forms.CharField(max_length=30, label="Titulo")
    descripcion = forms.CharField(widget=forms.Textarea,label="Descripción")
    requerido = forms.BooleanField(label="Tarea requerida")
    historial_Componente_ID = forms.CharField(max_length=30, label="Codigo componente contenedor")
    responsable = forms.CharField(max_length=30, label="Responsable tarea")
    fecha_Inicio = forms.DateTimeField(label="Fecha inicio")
    duracion = forms.IntegerField(label="Duración (Tiempo aprox. en días)") #Tiempo en Días
    actividad_ID = forms.CharField(max_length=30, label="Código actividad")
    estado = forms.IntegerField(label="Estado")