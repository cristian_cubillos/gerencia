from django.shortcuts import render
from rest_framework import viewsets
from .serializers import *

# Create your views here.

#Proyecto
class ProyectoViewSet (viewsets.ModelViewSet):
    
    serializer_class = ProyectoSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        return render(request, 'proyecto.html') 

#Requerimientos del proyecto
class RequerimientoViewSet (viewsets.ModelViewSet):
    
    serializer_class = RequerimientoSerializer
    #permission_classes = [] Solicitud de permisos...

#Elementos Flujo
class ElementoFlujoViewSet (viewsets.ModelViewSet):
    
    serializer_class = ElementoFlujoSerializer
    #permission_classes = [] Solicitud de permisos...

#Historial Componentes
class HistorialComponenteViewSet (viewsets.ModelViewSet):
    
    serializer_class = HistorialComponenteSerializer
    #permission_classes = [] Solicitud de permisos...

#Tarea
class TareaViewSet (viewsets.ModelViewSet):
    
    serializer_class = TareaSerializer
    #permission_classes = [] Solicitud de permisos...
