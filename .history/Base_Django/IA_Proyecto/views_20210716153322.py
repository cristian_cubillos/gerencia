from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib import messages
from rest_framework import viewsets
from .serializers import *
from .models import *
from .forms import *
from datetime import datetime

# Create your views here.

#Proyecto
class ProyectoViewSet (viewsets.ModelViewSet):
    
    serializer_class = ProyectoSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_Proyecto = Proyecto.objects.all()
        contexto= {
            'proyectos': lista_Proyecto,
        }
        return render(request, 'proyecto.html', contexto) 

    def crear_proyecto(request):
        if request.POST:
            form = ProyectoForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Proyecto Creado con éxito...')
                return redirect('../inicio_Proyecto/')
        else:
            form = ProyectoForm()
            contexto = {
                'proyectoForm' : form,
            }
            return render(request, 'crear_proyecto.html', contexto)

    def editar_proyecto(request, pk):
        post = get_object_or_404(Proyecto, pk=pk)
        if request.method == "POST":
            form = ProyectoForm(request.POST, instance=post)
            if form.is_valid():
                form.save()
                messages.success(request, 'Proyecto editado con éxito...')
                return redirect('../inicio_Proyecto/')
        else:
            form = ProyectoForm(instance=post)
        return render(request, 'crear_proyecto.html', {'proyectoEditForm': form})
             

#Requerimientos del proyecto
class RequerimientoViewSet (viewsets.ModelViewSet):
    
    serializer_class = RequerimientoSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_requerimientos = Requerimientos.objects.all()
        contexto= {
            'requerimientos': lista_requerimientos,
        }
        return render(request,'proyecto_requerimiento.html', contexto)

    def crear_requerimiento(request):
        if request.POST:
            form = RequerimientosForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Requerimiento Creado con éxito...')
                return redirect('../inicio_requerimiento/')
        else:            
            form = RequerimientosForm()
            contexto = {
                'requerimientoForm' : form,
            }
            return render(request, 'crear_requerimiento.html', contexto)

    def editar_requerimiento(request, pk):
        post = get_object_or_404(Requerimientos, pk=pk)
        if request.method == "POST":
            form = RequerimientosForm(request.POST, instance=post)
            if form.is_valid():
                form.save()
                messages.success(request, 'Requerimiento editado con éxito...')
                return redirect('../inicio_requerimiento/')
        else:
            form = RequerimientosForm(instance=post)
        return render(request, 'crear_proyecto.html', {'requerimientoFormEdit': form})

#Elementos Flujo
class ElementoFlujoViewSet (viewsets.ModelViewSet):
    
    serializer_class = ElementoFlujoSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_flujo = Elemento_Flujo.objects.all()
        contexto= {
            'procesos': lista_flujo,
        }
        return render(request,'proyecto_flujo.html', contexto)

    def crear_elemento(request):
        if request.POST:
            form = Elemento_FlujoForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Elemento creado con éxito...')
                return redirect('../inicio_elemento/')
        else:
            form = Elemento_FlujoForm()
            contexto = {
                'elementoForm' : form,
            }
            return render(request, 'crear_elemento.html', contexto)

    def editar_elemento(request, pk):
        post = get_object_or_404(Elemento_Flujo, pk=pk)
        if request.method == "POST":
            form = Elemento_FlujoForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                messages.success(request, 'Elemento editado con éxito...')
                return redirect('../inicio_elemento/')
        else:
            form = Elemento_FlujoForm(instance=post)
        return render(request, 'crear_elemento.html', {'elementoEditForm': form})

#Historial Componentes
class HistorialComponenteViewSet (viewsets.ModelViewSet):
    
    serializer_class = HistorialComponenteSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_historial = Historial_Componente.objects.all()
        contexto= {
            'historial': lista_historial,
        }
        return render(request,'proyecto_historial.html', contexto)

    def crear_historial(request):
        if request.POST:
            form = Historial_ComponenteForm(request.POST)
            if form.is_valid:
                form.save()
                return render(request, 'index.html')
        else:
            form = Historial_ComponenteForm()
            contexto = {
                'historialForm' : form,
            }
            return render(request, 'crear_historial.html', contexto)

    def editar_historial(request, pk):
        post = get_object_or_404(Elemento_Flujo, pk=pk)
        if request.method == "POST":
            form = Elemento_FlujoForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                lista_flujo = Elemento_Flujo.objects.all()
                contexto={
                    'message' : "Proceso guardado con éxito...",
                    'procesos' : lista_flujo,
                }
                return redirect('proyecto/inicio_flujo', contexto)
        else:
            form = Elemento_FlujoForm(instance=post)
        return render(request, 'crear_elemento.html', {'elementoEditForm': form})

#Tarea
class TareaViewSet (viewsets.ModelViewSet):
    
    serializer_class = TareaSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_tarea(request):
        if request.POST:
            form = TareaForm(request.POST)
            if form.is_valid:
                form.save()
                return render(request, 'index.html')
        else:
            form = TareaForm()
            contexto = {
                'tareaForm' : form,
            }
            return render(request, 'crear_tarea.html', contexto)

