from django import forms
from django.forms import fields
from django.forms.widgets import Textarea
from .models import *

#Proyecto
class ProyectoForm(forms.ModelForm):
    # titulo = forms.CharField(max_length=50, label="Titulo")
    # responsable = forms.CharField(max_length=50, label="Responsable")
    # cliente = forms.CharField(max_length=50, label="Cliente")
    # objetivo = forms.CharField(widget=forms.Textarea(attrs={ 'rows': 5 }),label="Objetivo")
    # descripcion = forms.CharField(widget=forms.Textarea(attrs={ 'rows': 5 }),label="Descripción")
    # estado = forms.ChoiceField(widget=forms.RadioSelect, 
    #     choices=[
    #         (1, 'En proceso'),
    #         (2, 'En espera'),
    #         (3, 'Aprobado'),
    #         (4, 'Archivado'),
    #     ],label="Estado")

    class Meta:
        model = Proyecto
        fields = ('titulo', 'responsable', 'cliente', 'cliente',
            'objetivo', 'descripcion', 'estado')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['responsable'].queryset = Usuario.objects.all()
        self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
                                choices=[
                                    (1, 'En proceso'),
                                    (2, 'En espera'),
                                    (3, 'Aprobado'),
                                    (4, 'Archivado'),
                                ],label="Estado")


#Requerimientos del proyecto
class RequerimientosForm(forms.ModelForm):
    # requeimiento = forms.CharField(widget=forms.Textarea(attrs={ 'size': 50 }),label="Requerimiento")
    # proyecto_ID = forms.CharField(max_length=30, label="Código proyecto")
    # estado = forms.IntegerField(label="Estado")

    class Meta:
        model = Requerimientos
        fields = ('requerimiento', 'proyecto_ID', 'estado')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['proyecto_ID'].queryset = Proyecto.objects.all()
        self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
                                choices=[
                                    (1, 'Activo'),
                                    (2, 'Inactivo'),
                                ],label="Estado")
        

#Elementos del Flujo
class Elemento_FlujoForm(forms.ModelForm):
    # flujo_ID = forms.CharField(max_length=30, label="Código WorkFlow")
    # proyecto_ID = forms.CharField(max_length=30, label="Código Proyecto")
    # responsable = forms.CharField(max_length=30, label="Responsable")
    # fecha_Inicio = forms.DateTimeField(label="Fecha inicio")
    # fecha_Vencimiento = forms.DateTimeField(label="Fecha vencimiento")
    # estado = forms.IntegerField(label="Estado")

    class Meta:
        model = Elemento_Flujo
        fields = ('flujo_ID', 'proyecto_ID', 'responsable',
            'fecha_Inicio', 'fecha_Vencimiento', 'estado')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['flujo_ID'].queryset = Flujo.objects.all()
        self.fields['proyecto_ID'].queryset = Proyecto.objects.all()
        self.fields['responsable'].queryset = Usuario.objects.all()
        self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
                                choices=[
                                    (1, 'Activo'),
                                    (2, 'Inactivo'),
                                ],label="Estado")

#Historial_Componente
class Historial_ComponenteForm(forms.ModelForm):
    # Componente_ID = forms.CharField(max_length=30, label="Código componente")
    # elemento_Flujo_ID = forms.CharField(max_length=30, label="Código elemento del flujo")
    # fecha_Elemento = forms.DateTimeField(label="Fecha del elemento")
    # estado = forms.IntegerField(label="Estado")

    class Meta:
        model = Historial_Componente
        fields = ('Componente_ID', 'elemento_Flujo_ID',
             'fecha_Elemento' ,'estado')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['Componente_ID'].queryset = Componente.objects.all()
        self.fields['elemento_Flujo_ID'].queryset = Elemento_Flujo.objects.all()
        self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
                                choices=[
                                    (1, 'Activo'),
                                    (2, 'Inactivo'),
                                ],label="Estado")

#Tarea
class TareaForm(forms.ModelForm):
    # nombre = forms.CharField(max_length=30, label="Titulo")
    # descripcion = forms.CharField(widget=forms.Textarea,label="Descripción")
    # requerido = forms.BooleanField(label="Tarea requerida")
    # historial_Componente_ID = forms.CharField(max_length=30, label="Codigo componente contenedor")
    # responsable = forms.CharField(max_length=30, label="Responsable tarea")
    # fecha_Inicio = forms.DateTimeField(label="Fecha inicio")
    # duracion = forms.IntegerField(label="Duración (Tiempo aprox. en días)") #Tiempo en Días
    # actividad_ID = forms.CharField(max_length=30, label="Código actividad")
    # estado = forms.IntegerField(label="Estado")

    class Meta:
        model = Tarea
        fields = ('nombre', 'descripcion', 'requerido',
             'historial_Componente_ID' ,'responsable',
             'fecha_Inicio', 'duracion', 'actividad_ID', 'estado')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['historial_Componente_ID'].queryset = Historial_Componente.objects.all()
        self.fields['responsable'].queryset = Usuario.objects.all()
        self.fields['actividad_ID'].queryset = Actividad.objects.all()
        self.fields['requerido'] = forms.BooleanField()
        self.fields['estado'] = forms.ChoiceField(widget=forms.RadioSelect, 
                                choices=[
                                    (1, 'Activo'),
                                    (2, 'Inactivo'),
                                ],label="Estado")
