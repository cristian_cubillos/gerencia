from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()

urlpatterns = router.urls + [
    path(r'inicio_Proyecto/', ProyectoViewSet.redic, name="proyecto/inicio_Proyecto"),
    path('kanban/', ProyectoViewSet.kanban, name="proyecto/kanban"),
    # path('monitor/porcentajes', MonitoreoIAViewSet.as_view({'post':'porcentajes'})),
]