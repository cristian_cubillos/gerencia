from django import forms
from django.forms.forms import Form

#Proyecto
class ProyectoForm(forms.Form):
    titulo = forms.CharField(max_length=50, unique=True)
    responsable = forms.CharField(max_length=50, unique=True)
    cliente = forms.CharField(max_length=50, unique=True)
    objetivo = forms.TextField()
    descripcion = forms.TextField()
    estado = forms.IntegerField()
    fecha_Creacion = forms.DateField(verbose_name="fecha_Creacion")
    fecha_Update = forms.DateField(verbose_name="fecha_Update")

#Requerimientos del proyecto
class RequerimientosForm(forms.Form):
    requeimiento = forms.TextField()
    proyecto_ID = forms.CharField(max_length=30, unique=True)
    estado = forms.IntegerField()
    fecha_Creacion = forms.DateField(verbose_name="fecha_Creacion")
    fecha_Update = forms.DateField(verbose_name="fecha_Update")

    def __str__(self):
        return self.requeimiento

#Elementos del Flujo
class Elemento_FlujoForm(forms.Form):
    flujo_ID = forms.CharField(max_length=30, unique=True)
    proyecto_ID = forms.CharField(max_length=30, unique=True)
    responsable = forms.CharField(max_length=30, unique=True)
    fecha_Inicio = forms.DateTimeField()
    fecha_Vencimiento = forms.DateTimeField()
    estado = forms.IntegerField()
    fecha_Creacion = forms.DateField(verbose_name="fecha_Creacion")
    fecha_Update = forms.DateField(verbose_name="fecha_Update")


#Historial_Componente
class Historial_ComponenteForm(forms.Form):
    Componente_ID = forms.CharField(max_length=30, unique=True)
    elemento_Flujo_ID = forms.CharField(max_length=30, unique=True)
    fecha_Elemento = forms.DateTimeField()
    estado = forms.IntegerField()
    fecha_Creacion = forms.DateField(verbose_name="fecha_Creacion")
    fecha_Update = forms.DateField(verbose_name="fecha_Update")


#Tarea
class TareaForm(forms.Form):
    nombre = forms.CharField(max_length=30, unique=True)
    descripcion = forms.TextField()
    requerido = forms.BooleanField()
    historial_Componente_ID = forms.CharField(max_length=30, unique=True)
    responsable = forms.CharField(max_length=30, unique=True)
    fecha_Inicio = forms.DateTimeField()
    duracion = forms.IntegerField() #Tiempo en Días
    actividad_ID = forms.CharField(max_length=30, unique=True)
    estado = forms.IntegerField()
    fecha_Creacion = forms.DateField(verbose_name="fecha_Creacion")
    fecha_Update = forms.DateField(verbose_name="fecha_Update")