from django.shortcuts import render
from rest_framework import viewsets
from .serializers import *
from .models import *
from .forms import *
from datetime import datetime

# Create your views here.

#Proyecto
class ProyectoViewSet (viewsets.ModelViewSet):
    
    serializer_class = ProyectoSerializer
    #permission_classes = [] Solicitud de permisos...

    def redic(request):
        lista_Proyecto = Proyecto.objects.all()
        return render(request, 'proyecto.html') 

    def crear_proyecto(request):
        if request.POST:
            form = ProyectoForm(request.POST)
            if form.is_valid:
                proyecto = form.save(commit=False)
                return render(request, 'proyecto.html')
        else:
            form = ProyectoForm()
            contexto = {
                'proyectoForm' : form,
            }
            return render(request, 'crear_proyecto.html', contexto)
             

#Requerimientos del proyecto
class RequerimientoViewSet (viewsets.ModelViewSet):
    
    serializer_class = RequerimientoSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_requerimiento(request):
        if request.POST:
            form = RequerimientosForm(request.POST)
            if form.is_valid:
                requerimientos = form.save(commit=False)
                return render(request, 'crear_requerimiento.html')
        else:            
            form = RequerimientosForm()
            contexto = {
                'requerimientoForm' : form,
            }
            return render(request, 'crear_requerimiento.html', contexto)

#Elementos Flujo
class ElementoFlujoViewSet (viewsets.ModelViewSet):
    
    serializer_class = ElementoFlujoSerializer
    #permission_classes = [] Solicitud de permisos...

#Historial Componentes
class HistorialComponenteViewSet (viewsets.ModelViewSet):
    
    serializer_class = HistorialComponenteSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_historial(request):
        if request.POST:
            form = Historial_ComponenteForm(request.POST)
            if form.is_valid:
                historial = form.save(commit=False)
                return render(request, 'crear_historial.html')
        else:
            form = Historial_ComponenteForm()
            contexto = {
                'historialForm' : form,
            }
            return render(request, 'crear_historial.html', contexto)

#Tarea
class TareaViewSet (viewsets.ModelViewSet):
    
    serializer_class = TareaSerializer
    #permission_classes = [] Solicitud de permisos...

    def crear_tarea(request):
        if request.POST:
            form = TareaForm(request.POST)
            if form.is_valid:
                tarea = form.save(commit=False)
                return render(request, 'tareas.html')
        else:
            form = TareaForm()
            contexto = {
                'tareaForm' : form,
            }
            return render(request, 'crear_tarea.html', contexto)

