from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()

urlpatterns = router.urls + [
    path(r'inicio_Proyecto/', ProyectoViewSet.redic, name="proyecto/inicio_Proyecto"),
    path('tareas/', ProyectoViewSet.kanban, name="proyecto/tareas"),
    path('crear_proyecto/', ProyectoViewSet.kanban, name="proyecto/crear_proyecto"),
]