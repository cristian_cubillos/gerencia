from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Proyecto)
admin.site.register(Requerimientos)
admin.site.register(Elemento_Flujo)
admin.site.register(Historial_Componente)
admin.site.register(Tarea)