from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib import messages
from rest_framework import viewsets
from .serializers import *
from .models import *
from .forms import *
from datetime import datetime

# Create your views here.

#Proyecto
class ProyectoViewSet (viewsets.ModelViewSet):
    
    serializer_class = ProyectoSerializer
    #permission_classes = [] Solicitud de permisos...

    @login_required
    def redic(request):
        lista_Proyecto = Proyecto.objects.filter(responsable_ID=request.user.pk)
        contexto= {
            'proyectos': lista_Proyecto,
        }
        return render(request, 'proyecto.html', contexto) 

    @staff_member_required
    @login_required
    def crear_proyecto(request):
        if request.POST:
            form = ProyectoForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Proyecto Creado con éxito...')
                return redirect('../inicio_Proyecto/')
        else:
            form = ProyectoForm()
            contexto = {
                'proyectoForm' : form,
            }
            return render(request, 'crear_proyecto.html', contexto)

    @staff_member_required
    @login_required
    def editar_proyecto(request, pk):
        post = get_object_or_404(Proyecto, pk=pk)
        if request.method == "POST":
            form = ProyectoForm(request.POST, instance=post)
            if form.is_valid():
                form.save()
                messages.success(request, 'Proyecto editado con éxito...')
                return redirect('../inicio_Proyecto/')
        else:
            form = ProyectoForm(instance=post)
        return render(request, 'crear_proyecto.html', {'proyectoEditForm': form})
             

#Requerimientos del proyecto
class RequerimientoViewSet (viewsets.ModelViewSet):
    
    serializer_class = RequerimientoSerializer
    #permission_classes = [] Solicitud de permisos...

    @login_required
    def redic(request,pk):
        lista_requerimientos = Requerimientos.objects.filter(proyecto_ID_id=pk)
        contexto= {
            'requerimientos': lista_requerimientos,
        }
        return render(request,'proyecto_requerimiento.html', contexto)

    @staff_member_required
    @login_required
    def crear_requerimiento(request):
        if request.POST:
            form = RequerimientosForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Requerimiento Creado con éxito...')
                return redirect('../inicio_requerimiento/')
        else:            
            form = RequerimientosForm()
            contexto = {
                'requerimientoForm' : form,
            }
            return render(request, 'crear_requerimiento.html', contexto)

    @staff_member_required
    @login_required
    def editar_requerimiento(request, pk):
        post = get_object_or_404(Requerimientos, pk=pk)
        if request.method == "POST":
            form = RequerimientosForm(request.POST, instance=post)
            if form.is_valid():
                form.save()
                messages.success(request, 'Requerimiento editado con éxito...')
                return redirect('../inicio_requerimiento/')
        else:
            form = RequerimientosForm(instance=post)
        return render(request, 'crear_proyecto.html', {'requerimientoFormEdit': form})

#Elementos Flujo
class ElementoFlujoViewSet (viewsets.ModelViewSet):
    
    serializer_class = ElementoFlujoSerializer
    #permission_classes = [] Solicitud de permisos...

    @login_required
    def redic(request):
        lista_flujo = Elemento_Flujo.objects.all()
        contexto= {
            'procesos': lista_flujo,
        }
        return render(request,'proyecto_flujo.html', contexto)

    @staff_member_required
    @login_required
    def crear_elemento(request):
        if request.POST:
            form = Elemento_FlujoForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Elemento creado con éxito...')
                return redirect('../inicio_elemento/')
        else:
            form = Elemento_FlujoForm()
            contexto = {
                'elementoForm' : form,
            }
            return render(request, 'crear_elemento.html', contexto)

    @staff_member_required
    @login_required
    def editar_elemento(request, pk):
        post = get_object_or_404(Elemento_Flujo, pk=pk)
        if request.method == "POST":
            form = Elemento_FlujoForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                messages.success(request, 'Elemento editado con éxito...')
                return redirect('../inicio_elemento/')
        else:
            form = Elemento_FlujoForm(instance=post)
        return render(request, 'crear_elemento.html', {'elementoEditForm': form})

#Historial Componentes
class HistorialComponenteViewSet (viewsets.ModelViewSet):
    
    serializer_class = HistorialComponenteSerializer
    #permission_classes = [] Solicitud de permisos...

    @login_required
    def redic(request):
        lista_historial = Historial_Componente.objects.all()
        contexto= {
            'historial': lista_historial,
        }
        return render(request,'proyecto_historial.html', contexto)

    @staff_member_required
    @login_required
    def crear_historial(request):
        if request.POST:
            form = Historial_ComponenteForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Historial creado con éxito...')
                return redirect('../inicio_historial/')
        else:
            form = Historial_ComponenteForm()
            contexto = {
                'historialForm' : form,
            }
            return render(request, 'crear_historial.html', contexto)

    @staff_member_required
    @login_required
    def editar_historial(request, pk):
        post = get_object_or_404(Historial_Componente, pk=pk)
        if request.method == "POST":
            form = Historial_ComponenteForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                messages.success(request, 'Historial editado con éxito...')
                return redirect('../inicio_historial/')
        else:
            form = Elemento_FlujoForm(instance=post)
        return render(request, 'crear_historial.html', {'historialFormEdit': form})

#Tarea
class TareaViewSet (viewsets.ModelViewSet):
    
    serializer_class = TareaSerializer
    #permission_classes = [] Solicitud de permisos...

    @login_required
    def redic(request):
        lista_tarea = Tarea.objects.all()
        contexto= {
            'tareas': lista_tarea,
        }
        return render(request,'proyecto_tarea.html', contexto)

    @staff_member_required
    @login_required
    def crear_tarea(request):
        if request.POST:
            form = TareaForm(request.POST)
            if form.is_valid:
                form.save()
                messages.success(request, 'Tarea creada con éxito...')
                return redirect('../inicio_tarea/')
        else:
            form = TareaForm()
            contexto = {
                'tareaForm' : form,
            }
            return render(request, 'crear_tarea.html', contexto)

    @staff_member_required
    @login_required
    def editar_tarea(request, pk):
        post = get_object_or_404(Tarea, pk=pk)
        if request.method == "POST":
            form = TareaForm(request.POST, instance=post)
            if form.is_valid:
                form.save()
                messages.success(request, 'Tarea editada con éxito...')
                return redirect('../inicio_tarea/')
        else:
            form = TareaForm(instance=post)
        return render(request, 'crear_tarea.html', {'tareaFormEdit': form})