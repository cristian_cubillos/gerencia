from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import *

router = SimpleRouter()

urlpatterns = router.urls + [
    path(r'inicio_Proyecto/', ProyectoViewSet.redic, name="proyecto/inicio_Proyecto"),
    path('tareas/', ProyectoViewSet.kanban, name="proyecto/tareas"),
    path('crear_proyecto/', ProyectoViewSet.crear_proyecto, name="proyecto/crear_proyecto"),
    path('crear_historial_componente/', HistorialComponenteViewSet.crear_historial, name="proyecto/crear_historial_componente"),
]